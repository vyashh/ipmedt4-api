<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Auth::routes();

// -Club--------------------------------------------------------------------

// Alle clubs
Route::get('/clubs','ClubController@index');

// Alle clubs van aangegeven genre
Route::get('/clubs/genre/{genre}','ClubController@showGenre');

// Club met aangegeven id
// Wordt waarschijnlijk niet gebruikt
Route::get('/clubs/{id}','ClubController@showId');

// Club toevoegen
Route::post('/addClub', 'ClubController@addClub');


// -Clublid------------------------------------------------------------------

// Alle clubleden
Route::get('/clubleden','ClublidController@index');

// Admin clublid toevoegen
Route::post('/addAdmin/{clubNaam}','clublidController@addAdmin');

// Clubleden van aangegeven club
Route::get('/clubleden/{club}','ClublidController@show');

// Lidmaatschappen van aangegeven gebruiker
Route::get('/clubleden/gebruiker/{gebruiker}','ClublidController@showGebruikerLidmaatschappen');

// Aantal clubleden van aangegeven club
// Retourneerd een int
Route::get('/clubleden/{club}/aantal','ClublidController@aantalClubleden');

// Geeft aan of de clublid lid is van de club
// Retourneerd een boolean
Route::get('/clubleden/{club}/lid/{gebruiker}','ClublidController@checkOfGebruikerLidIs');

// Clublid toevoegen
Route::post('/addClublid','ClublidController@addClublid');

// Clublid verwijderen
Route::post('/deleteClubLid','ClublidController@deleteClubLid');

// Bevat alle informatie voor clubpagina
Route::get('/showAdmin/gebruiker/{gebruikerID}/club/{clubId}','clublidController@showAdmin');


// -Gebruiker---------------------------------------------------------------

// Alle gebruikers
Route::get('/gebruikers','UserController@index');

// Gebruiker toevoegen
Route::post('/user/add','UserController@addUser');


// -Boekenlijst-------------------------------------------------------------

// Route::get('/mylist','BoekInBoekenlijstController@index');
Route::get('/mylist/{id_user}/{id_book}','BoekInBoekenlijstController@getBook');
Route::post('/mylist/add','BoekInBoekenlijstController@addBook');
Route::get('/mylist/{id}','BoekInBoekenlijstController@index');
Route::get('/mylist/{id}/total','BoekInBoekenlijstController@getTotalBooks');
Route::get('/mylist/{id_book}/{id_user}', "BoekInBoekenlijstController@getProcent");

// -Chat---------------------------------------------------------------------

Route::post('/chat/add', 'ChatController@addMessage');
Route::get('/chat/get/{club}', 'ChatController@getMessages');


// -Overig------------------------------------------------------------------

Route::patch('/updatePagesRead/{boekId}/{userId}', 'BoekInBoekenlijstController@updatePagesRead');
// Deze routes kunnen waarschijnlijk verwijdert worden
Route::get('/genres','GenreController@index');
Route::get('/boeken_van_de_week','BoekVanDeWeekController@index');
Route::get('/gestemde_boeken_van_de_week','GestemdeBoekVanDeWeekController@index');

// Route::get('/', 'PageController@create');
