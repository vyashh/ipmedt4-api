<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClublidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clublid', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_club')->unsigned()->nullable();
            $table->foreign('id_club')->references('id')->on('club');
            $table->string('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->boolean('is_admin')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        schema::table('clublid', function($table){
          $table->dropForeign('clublid_id_club_foreign');
          $table->dropSoftDeletes();

        });

        schema::table('clublid', function($table){
          $table->dropForeign('clublid_id_user_foreign');
          $table->dropSoftDeletes();
        });

        Schema::dropIfExists('clublid');
    }
}