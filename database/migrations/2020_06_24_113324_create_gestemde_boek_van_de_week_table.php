<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestemdeBoekVanDeWeekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestemde_boek_van_de_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_boek_van_de_week')->unsigned();
            $table->foreign('id_boek_van_de_week')->references('id')->on('boek_van_de_week');
            $table->string('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('gestemde_boek_van_de_week', function($table){
          $table->dropForeign('gestemde_boek_van_de_week_id_boek_van_de_week_foreign');
        });

        schema::table('gestemde_boek_van_de_week', function($table){
          $table->dropForeign('gestemde_boek_van_de_week_id_user_foreign');
        });

        Schema::dropIfExists('gestemde_boek_van_de_week');
    }
}