<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoekVanDeWeekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boek_van_de_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_boek')->unsigned();
            $table->foreign('id_boek')->references('id')->on('boek');
            $table->integer('id_club')->unsigned();
            $table->foreign('id_club')->references('id')->on('club');
            //$table->string('gekozen_week');
            $table->boolean('gekozen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('boek_van_de_week', function($table){
          $table->dropForeign('boek_van_de_week_id_boek_foreign');
        });

        schema::table('boek_van_de_week', function($table){
          $table->dropForeign('boek_van_de_week_id_club_foreign');
        });

        Schema::dropIfExists('boek_van_de_week');
    }
}
