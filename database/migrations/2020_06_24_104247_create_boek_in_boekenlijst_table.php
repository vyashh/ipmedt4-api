<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoekInBoekenlijstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boek_in_boekenlijst', function (Blueprint $table) {
            $table->string('title');
            $table->increments('id');
            $table->string('id_boek')->unique();
            $table->string('authors');
            $table->string('thumbnail');
            $table->string('average_rating')->nullable();
            $table->string('categories')->nullable();
            $table->string('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->integer('pages_total')->nullable();
            $table->integer('pages_read')->nullable();
            $table->integer('procent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // schema::table('boek_in_boekenlijst', function($table){
        //   $table->dropForeign('boek_in_boekenlijst_id_user_foreign');
        // });

        Schema::dropIfExists('boek_in_boekenlijst');
    }
}
