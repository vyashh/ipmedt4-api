<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naam')->unique();
            $table->string('beschrijving');
            $table->string('genre')->nullable(true); //tijdelijk true moet uiteindelijk false zijn
            $table->foreign('genre')->references('genre_naam')->on("genre");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('club', function($table){
          $table->dropForeign('club_genre_foreign');
        });

        Schema::dropIfExists('club');
    }
}
