<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boek', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn');
            $table->string('titel');
            $table->string('schrijver');
            $table->string('afbeelding');
            $table->integer('jaar');
            $table->string('genre');
            $table->foreign('genre')->references('genre_naam')->on("genre");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('boek', function($table){
          $table->dropForeign('boek_genre_foreign');
        });

        Schema::dropIfExists('boek');
    }
}
