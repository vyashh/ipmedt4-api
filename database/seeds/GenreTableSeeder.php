<?php

use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genre')->insert([
          "genre_naam" => "fantasy",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "mystery",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "scifi",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "literature",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "biography",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "horror",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "romance",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "novel",
        ]);

        DB::table('genre')->insert([
          "genre_naam" => "poetry",
        ]);
    }
}
