<?php

use Illuminate\Database\Seeder;

class ClublidTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clublid')->insert([
          "id_club" => "1",
          "id_user" => "rYZv3tijWcWzAN9cFVh9anLOZAs1",
        ]);

        DB::table('clublid')->insert([
          "id_club" => "1",
          "id_user" => "yXDePdIAlNRJAY2gooTs1ZrYXoq1",
          "is_admin" => "1",
        ]);

        DB::table('clublid')->insert([
          "id_club" => "1",
          "id_user" => "APf8YKHQnnTq2cKgZ6pR7lR7UxG3",
          "is_admin" => "1",
        ]);
    }
}