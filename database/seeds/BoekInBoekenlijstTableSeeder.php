<?php

use Illuminate\Database\Seeder;

class BoekInBoekenlijstTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('boek_in_boekenlijst')->insert([
          "id_boek" => "Lqxe3O8EJP0C",
          "id_user" => "rYZv3tijWcWzAN9cFVh9anLOZAs1",
          "pages_total" => 141,
        ]);
    }
}