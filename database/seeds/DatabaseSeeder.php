<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(GenreTableSeeder::class);
        $this->call(ClubTableSeeder::class);
        $this->call(ClublidTableSeeder::class);
        $this->call(BoekTableSeeder::class);
//        $this->call(BoekInBoekenlijstTableSeeder::class);
        $this->call(BoekVanDeWeekTableSeeder::class);
        $this->call(GestemdeBoekVanDeWeekTableSeeder::class);
    }
}
