<?php

use Illuminate\Database\Seeder;

class GestemdeBoekVanDeWeekTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gestemde_boek_van_de_week')->insert([
          "id_boek_van_de_week" => "1",
          "id_user" => "rYZv3tijWcWzAN9cFVh9anLOZAs1",
        ]);
    }
}