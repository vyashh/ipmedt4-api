<?php

use Illuminate\Database\Seeder;

class BoekTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('boek')->insert([
          "isbn" => "12334567891234",
          "titel" => "Game of Thrones",
          "schrijver" => "George R. R. Martin",
          "afbeelding" => "afbeelding",
          "jaar" => "1996",
          "genre" => "fantasy",
        ]);
    }
}
