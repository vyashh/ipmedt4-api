<?php

use Illuminate\Database\Seeder;

class ClubTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('club')->insert([
          "naam" => "A Song Of Ice And Fire",
          "beschrijving" => "Discusse ASOIAF boeken.",
          "genre" => "fantasy",
        ]);

        DB::table('club')->insert([
          "naam" => "Test",
          "beschrijving" => "Beschrijving van club.",
          "genre" => "horror",
        ]);
    }
}
