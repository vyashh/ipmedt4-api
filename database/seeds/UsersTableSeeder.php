<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          "id" => "rYZv3tijWcWzAN9cFVh9anLOZAs1",
          "display_name" => "Vyash Bhawan",
        ]);

        DB::table('users')->insert([
            "id" => "yXDePdIAlNRJAY2gooTs1ZrYXoq1",
            "display_name" => "Tijs Ruigrok",
          ]);

        DB::table('users')->insert([
          "id" => "APf8YKHQnnTq2cKgZ6pR7lR7UxG3",
          "display_name" => "Damion Uijldert",
        ]);

        DB::table('users')->insert([
          "id" => "wsMgSahAIlRv1x82e4Z4s9amzBt1",
          "display_name" => "Shihayna Poulina",
        ]);
    }
}