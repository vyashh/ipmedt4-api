<?php

use Illuminate\Database\Seeder;

class BoekVanDeWeekTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('boek_van_de_week')->insert([
          "id_boek" => "1",
          "id_club" => "1",
          "gekozen" => False,
        ]);
    }
}
