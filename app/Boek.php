<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boek extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'boek';
}
