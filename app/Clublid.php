<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Clublid extends Model
{

    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'clublid';
    public $timestamps = false;

    public function getClub(){
      return $this->hasOne('App\Club','club_id','id');
    }

    public function getUser(){
      return $this->hasOne('App\Club','user_id','id');
    }
}