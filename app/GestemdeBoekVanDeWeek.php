<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GestemdeBoekVanDeWeek extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'gestemde_boek_van_de_week';
}
