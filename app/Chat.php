<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'chat';
    public $timestamps = true;

    // public function getMessage(){
    //     return $this->hasOne('App\Chat','chat','message');
    //   }
}
