<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoekInBoekenlijst extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'boek_in_boekenlijst';
    public $timestamps = false;
}
