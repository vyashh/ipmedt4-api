<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use DB;

class GenreController extends Controller
{
    public function index(){
      return Genre::all();
    }
}
