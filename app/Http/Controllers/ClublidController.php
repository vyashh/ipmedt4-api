<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clublid;
use DB;

class ClublidController extends Controller
{
    public function index(){
      return Clublid::all();
    }

    public function show($club){
      return Clublid::where('id_club', $club)->get();
    }

    public function showGebruikerLidmaatschappen($gebruiker){
      
      return Clublid::where('id_user', '=', $gebruiker)->get();
    }

    public function aantalClubleden($club){
      $clubledenLijst = Clublid::where('id_club','=',$club)->get();
      return $clubledenLijst->count();
    }

    public function addClublid(Request $request){
      $clublid = new Clublid();
      $clublid->id_club = $request->id_club;
      $clublid->id_user = $request->id_user;
      $clublid->save();
    }

    public function deleteClubLid(Request $request){
      Clublid::where('id_club','=',$request->id_club)->where('id_user','=',$request->id_user)->forceDelete();
      return response()->json([
        'message' => "user unjoined",
        'request' => $request->id_user
      ], 200);
    }

    public function checkOfGebruikerLidIs($club, $lid){

      $joined = null ;
      if (Clublid::where('id_club','=',$club)->where('id_user','=',$lid)->exists()) {
        $joined = TRUE;
      } else {
        $joined = FALSE;
      }

      return response()->json([
        'message' => $joined,
      ], 200);
    }

    public function addAdmin(Request $request, $clubNaam){
      $clubId = DB::table('club')
        ->where('club.naam','=',$clubNaam)
        ->pluck('id')
        ->first();

      $clublid = new Clublid();
      $clublid->id_club = $clubId;
      $clublid->id_user = $request->id_user;
      $clublid->is_admin = 1;
      $clublid->save();
    }

    public function showAdmin($gebruikerId, $clubId){
      return Clublid::where('id_user','=',$gebruikerId)
        ->join('club','clublid.id_club','=','club.id')
        ->select('club.*','clublid.is_admin')
        ->where('club.id','=',$clubId)
        ->first();
    }

}
