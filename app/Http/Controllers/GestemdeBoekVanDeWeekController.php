<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GestemdeBoekVanDeWeek;
use DB;

class GestemdeBoekVanDeWeekController extends Controller
{
    public function index(){
      return GestemdeBoekVanDeWeek::all();
    }
}
