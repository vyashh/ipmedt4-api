<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Club;
use App\Clublid;
use DB;

class ClubController extends Controller
{
    public function index(){
      return Club::all();
    }

    public function showGenre($genre){
      if(strtolower($genre) == 'all') {
        return Club::all();
      }
      else {
        return Club::where('genre','=',$genre)->get();
      }
    }

    public function showId($id){
      return Club::where('id','=',$id)->get();
     }

    public function addClub(Request $request){
       $club = new Club();
       $club->naam = $request->naam;
       $club->beschrijving = $request->beschrijving;
       
       $genre = $request->genre;
       $club->genre = strtolower($genre);
       
       $club->save();

       return response()->json([
        'message' => "New club created! Check your profile to manage your club.",
      ], 200);
     }
}