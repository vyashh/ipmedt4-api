<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BoekInBoekenlijst;
use DB;

class BoekInBoekenlijstController extends Controller
{
    public function index($id){
      return BoekInBoekenlijst::where('id_user', $id)->get();
    }

    public function addBook(Request $request){
      $book = new BoekInBoekenlijst();
      $book->title = $request->title;
      $book->id_boek = $request->id_boek;
      $book->id_user = $request->id_user;
      $book->thumbnail = $request->thumbnail;
      $book->authors = $request->authors;
      $book->average_rating = $request->averageRating;
      $book->pages_total = $request->pages;
      $book->categories= $request->categories;
      $book->procent = $request->procent;

      $book->save();

      return response()->json([
        'book' => $request->boek_id,
        'message' => 'Success'
      ], 200);
    }

    public function getTotalBooks($id){
      return BoekInBoekenlijst::where('id_user', $id)->count();
    }

    public function getBook($id_book, $id_user){
        return BoekInBoekenlijst::where('id_boek', $id_book)
            ->where('id_user', $id_user)
            ->get();
    }

    public function updatePagesRead(Request $request, $boekId, $userId){
        BoekInBoekenlijst::where('id', $boekId)
            ->where('id_user',$userId)
            ->update(['pages_read' => $request->pages_read]);
    }

    public function getProcent($procent, $id_book){
      return BoekInBoekenlijst::where('id_boek', $id_book)
      ->where('procent', $procent)
      ->get();
    }
}
