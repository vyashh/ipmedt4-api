<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Database\QueryException;

class UserController extends Controller
{
    function index(){
        return User::all();
    }

    function addUser(Request $request){
        $user = new User();
        $user->id = $request->uid;
        $user->display_name = $request->displayName;
        $user->save();
        
        return response()->json([
            'user' => $request->uid,
            'message' => 'Success'
          ], 200);
    }
}