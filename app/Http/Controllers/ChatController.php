<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use DB;

class ChatController extends Controller
{
    public function index(){
        return Chat::all();
    }

    public function addMessage(Request $request){
        $chat = new Chat();
        $chat->id_user = $request->id_user;
        $chat->display_name = $request->display_name;
        $chat->id_club = $request->id_club;
        $chat->message = $request->message;
        $chat->save();
        
        return response()->json([
            'message' => "chat saved",
          ], 200);
    }
    
    public function getMessages($id){
        return Chat::where('id_club','=',$id)->get();
    }
}