<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'club';
    public $timestamps = false;

    public function getGenre(){
      return $this->hasOne('App\Genre','genre','genre_naam');
    }

}
