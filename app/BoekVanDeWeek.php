<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoekVanDeWeek extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'boek_van_de_week';
}
